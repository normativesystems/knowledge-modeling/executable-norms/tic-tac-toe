#    Copyright 2023 Flores Bakker & Nederlandse Organisatie voor Toegepast Natuur-
#     wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for 
#     applied scientific research
import os
import argparse
from rdflib import Graph
import pyshacl

# You can call ScenarioScript.py for a specific scenario, as follows:
#
#   python ScenarioScript.py --scenario 2
#
# This should print a statement that the script will run scenario 2.

# Set the path to the desired standard directory
directory_path = "C:/Users/Administrator/Documents/Branches/AI4Legs"

# Set the standard directory
os.chdir(directory_path)

def read_and_append_strings(file_paths, existing_string):
    global directory_path
    combined_string = existing_string
    for file_path in file_paths:
        file_path = directory_path + file_path
        # Open each file in read mode
        with open(file_path, 'r') as file:
            # Read the content of the file and append it to the existing string
            file_content = file.read()
            combined_string += file_content

    return combined_string

arg_parser = argparse.ArgumentParser(description='Process some integers.')
arg_parser.add_argument("--scenario",
    choices=range(1,20),
    default=9,
    type=int,
    help='the number of the scenario to run.'
)
args = arg_parser.parse_args()
# Read the arg
try:
    if args.scenario:
        scenario_number = args.scenario
        print("Running scenario " + str(scenario_number))
except:
    print("Failed to process command line argument, defaulting to scenario 9")
    scenario_number = 9



# Read the shapes
file_paths = ["/tic-tac-toe/data/static/model/TicTacToe - Shapes.ttl"]
shapes_graph_seed = ""
shapes_graph = read_and_append_strings(file_paths, shapes_graph_seed)

# Load the specifications and scenario as RDF graphs
file_paths = [
        "/tic-tac-toe/data/static/model/Flint model.ttl",
        "/tic-tac-toe/data/static/model/TicTacToe - ActFrames.ttl",
        "/tic-tac-toe/data/static/model/TicTacToe - FactFrames.ttl",
        "/tic-tac-toe/data/static/model/TicTacToe - Mapping.ttl",
        "/tic-tac-toe/data/static/scenarios/scenario" + str(scenario_number) + ".ttl",
        "/tic-tac-toe/data/static/scenarios/TicTacToe - ScenarioMetadata.ttl",
    ]
data_graph_seed = ""
data_graph_string = read_and_append_strings(file_paths, data_graph_seed)
data_graph = Graph().parse(data=data_graph_string, format="turtle")
# Run SHACL validation on the graph
# This will also apply inferencing and SPARQL CONSTRUCT to generate the new state
results = pyshacl.validate(
    data_graph,
    shacl_graph=shapes_graph,
    data_graph_format="turtle",
    shacl_graph_format="turtle",
    advanced=True,
    inplace=True,
    inference="owlrl",
    debug=False,
    serialize_report_graph="turtle",
)

# Print validation results and save the resulting graph.
conforms, report_graph, report_text = results
print(report_text)
print(data_graph.serialize(destination=directory_path+'/tic-tac-toe/data/generated/scenario/TicTacToe_ScenarioResult' + str(scenario_number) + ".ttl", format='turtle'))
