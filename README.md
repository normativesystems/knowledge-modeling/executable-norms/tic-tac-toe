# Executable Tic-tac-toe

A demonstration of an executable normative system based on semantic web technology for the case of playing a game of tic-tac-toe.

This repository is based on previous work by @fbakker and TNO. This work was presented at ICAIL 2023, AI4Legs workshop. See https://site.unibo.it/hypermodelex/en/agenda/ai4legs-2023.

## Introduction

This repository contains two scripts, a scenario script and a game script. Both scripts make use of a semantic web compliant norm engine. The scenario script lets you evaluate a specific scenario in a game of Tic-Tac-Toe against norms. The game script lets you play a complete game of Tic-Tac-Toe with another player, where each step in the game is evaluated using the norm engine.   
 
## Scenario script
 
A script with which a scenario can be normatively evaluated. In total, fifteen different scenarios were developed that can be validated by the norm engine. Nine scenarios involve compliant behavior, in which the actor is showing behavior that is deemed permissible according to the norms. It is expected that the norm engine will validate this behavior and create a specific post-scenario, as the behavior is allowed to have the intended effect on institutional reality. In five scenarios, non-compliant behavior is involved, for which the norm engine is expected to indicate this non-compliance accordingly. In those scenarios the engine will flag the scenarios as non-compliant and prevent the behavior to have the intended effect on institutional reality (i.e., ex-ante evaluation). Lastly, a hybrid scenario involves both compliant and non-compliant behavior, where a punitive rule is invoked to handle an earlier established non-compliance.
	
### installing packages
			
Make sure you have installed PyShacl, RDFlib and OS. Use the command "pip install rdflib" for example in the command prompt. See the respective documentation of these libraries.
			
### running the script
			
Run the following line in the command prompt (example for scenario 2):
			
*python ScenarioScript.py --scenario 2*
 
## Scenario description

### Compliant scenario

1. Player X starts a game of Tic-Tac-Toe.

*Player X starts a game of Tic-Tac-Toe on a clean gameboard. Result of the act is a state change where a square has been marked with the symbol X, the game is running from now on and that player O now has the turn.*
   
2. Player O takes its first turn in a game of Tic-Tac-Toe.

*Player O takes its first turn in a game of Tic-Tac-Toe. Result of the act is a state change where a new square has been marked with the symbol O, the game is still running and that player X now has the turn.*

3. Player X takes its first turn in a game of Tic-Tac-Toe.

*Player X takes its first turn (after having started it earlier) in a game of Tic-Tac-Toe. Result of the act is a state change where a new square has been marked with the symbol X, the game is still running and that player O now has the turn.*

4. Player O takes its second turn in a game of Tic-Tac-Toe.

*Player O takes its second turn in a game of Tic-Tac-Toe. Result of the act is a state change where a new square has been marked with the symbol O, the game is still running and that player X now has the turn.*

5. Player X takes its second turn in a game of Tic-Tac-Toe.

*Player X takes its second turn in a game of Tic-Tac-Toe. Result of the act is a state change where a new square has been marked with the symbol X, the game is still running and that player O now has the turn.*

6. Player O takes its third turn in a game of Tic-Tac-Toe.

*Player O takes its third turn in a game of Tic-Tac-Toe. Result of the act is a state change where a new square has been marked with the symbol O, the game is still running and that player X now has the turn.*

7. Player X takes its third turn in a game of Tic-Tac-Toe.

*Player X takes its third turn in a game of Tic-Tac-Toe. Result of the act is a state change where a new square has been marked with the symbol X, the game is still running and that player O now has the turn. Note how this act creates three in a row for player X.*

8. Player X declares a win.

*Player X declares victory as there is three in a row for player X. Result is a state change where the game has ended and player X is awarded victory.*

9. Player O declares a tie.

*Player O declares a tie as there is no three in a row for a player and there are no more squares to be filled. Result is a state change where the game has ended and none is awarded victory.*

### Detection of non-compliant behavior (ex-ante evaluation)

10. Player X incorrectly declares player X has won while player O has three squares in a row.

*Result: The norm engine detects the non-compliance and reports that the winning conditions for this Tic-Tac-Toe game have not been met for this actor. The actor must have at least three squares in a row marked with the same symbol. No state change will have occured.*

11. Player X incorrectly takes turn in a game of Tic-Tac-Toe while player O already has three squares in a row.

*Result: The norm engine detects the non-compliance and reports that this act is not valid as it seems a player already has won due to the fact that there are three squares in a row marked with the same symbol. No state change will have occured.*

12. Player O incorrectly declares a tie while there are still empty squares.

*Result: The norm engine detects the non-compliance and reports that not all of the squares of the game board are marked. Hence, the game is still on going and the player who has the turn has still to make a move. No state change will have occured.*

13. Player O incorrectly declares a tie while player X has three squares in a row.

*Result: The norm engine detects the non-compliance and reports that this act is not valid as it seems a player already has won due to the fact that there are three squares in a row marked with the same symbol. No state change will have occured.*

14. Player O incorrectly takes turn in a game of Tic-Tac-Toe but does not have the turn.

*Result: The norm engine detects the non-compliance and reports that the current move is not allowed, as the player does not have the turn. No state change will have occured.*

### Handling of non-compliant behavior: punitive scenario

15. Player X correctly declares foul play by player O and is thus awarded victory due to non-compliance of player O.

*Player X declares foul play by player O as player O has taken a turn when that player did not have the turn. Result of the act is a state change where the game has ended and player X is awarded victory.*
 
## Game script
 
A script with which one can play a complete game of Tic-Tac-Toe against one another, where the evaluation of each turn is done by the norm engine. The script is fairly rudimentary and unforgiving for faulty input. 
 
### installing packages
			
Make sure you have installed PyShacl, RDFlib and OS. Use the command "pip install rdflib" for example in the command prompt. See the respective documentation of these libraries.
			
### running the script
			
Run the following line in the command prompt:
			
*python GameScript.py*

Now you can enter game actions for both players and play a complete game of Tic-Tac-Toe. 
			
