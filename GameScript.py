import os
from rdflib import Graph, Namespace
from rdflib.namespace import RDF, PROV
import pyshacl

# Set the path to the desired standard directory
directory_path = "C:/Users/Administrator/Documents/Branches/AI4Legs"

# Set the standard directory
os.chdir(directory_path)

def copy_file(source_file, destination_file):
    global directory_path
    file_path_source = directory_path + source_file
    file_path_destination = directory_path + destination_file
    with open(file_path_source, 'r') as source:
        with open(file_path_destination, 'w') as destination:
            # Read the contents of the source file
            contents = source.read()          
            # Write the contents to the destination file
            destination.write(contents)

def make_file_empty(source_file):
    global directory_path
    file_path = directory_path + source_file
    with open(file_path, 'w') as file:
        file.truncate(0)

def read_and_append_strings(file_paths, existing_string):
    global directory_path
    combined_string = existing_string
    for file_path in file_paths:
        file_path = directory_path + file_path
        # Open each file in read mode
        with open(file_path, 'r') as file:
            # Read the content of the file and append it to the existing string
            file_content = file.read()
            combined_string += file_content

    return combined_string

def append_file_contents(source_file, destination_file):
    global directory_path
    file_path_source = directory_path + source_file
    file_path_destination = directory_path + destination_file
    # Open both files in the appropriate mode
    with open(file_path_source, 'r') as file1, open(file_path_destination, 'a') as file2:
        # Read the content of the first file
        content = file1.read()

        # Write the content of the first file to the second file
        file2.write(content)

def add_triples(source_graph, destination_graph):
    for triple in source_graph:
        if triple not in destination_graph:
            destination_graph.add(triple)

# Function to perform a player's move
def execute_move():
    tictactoe_actor = None
    tictactoe_action = None
    tictactoe_object = None
    tictactoe_opponent = None
    tictactoe_symbol = None
        
    tictactoe_actor, tictactoe_action, tictactoe_object = input("""
   
   Choose your next move:
       
   Player role: [ x | o ]
   Type of action: [ mark | declare ]
   Type of object: [ 1-9 | win | tie | ]
       
   Example: 
       x mark 1
       o declare tie
       
    Enter: """).split()
    
    tictactoe_actor = tictactoe_actor.upper()
    tictactoe_action = tictactoe_action.lower()
    tictactoe_object = tictactoe_object.lower()
    tictactoe_opponent = 'X' if tictactoe_actor == 'O' else 'O'
    tictactoe_symbol = 'X' if tictactoe_actor == 'X' else 'O'
    
    print ("")
    print ("--------")
    print ("Scenario")
    print ("--------")
    print ("actor   : ", tictactoe_actor)
    print ("action  : ", tictactoe_action)
    print ("object  : ", tictactoe_object)
    print ("opponent: ", tictactoe_opponent)
    print ("symbol  : ", tictactoe_symbol)
    print ("--------")
    print ("")
    
    # Establish the current scenario ID and create player action template
    CurrentGameState_graph = None
    CurrentGameState_graph = Graph()
    CurrentGameState_graph.bind("flint", flint)
    CurrentGameState_graph.bind("fact", fact)
    CurrentGameState_graph.bind("scenario", scenario)
    CurrentGameState_graph.bind("RDF", RDF)
    CurrentGameState_graph.bind("prov", PROV)

    CurrentGameState_graph.parse("tic-tac-toe/data/generated/game/TicTacToe - ValidationHistory.ttl", format="turtle")
    
    square_iri = None
    
    if tictactoe_action == 'mark':
        # Read the SPARQL query to establish square id
        file_paths = ["/tic-tac-toe/queries/TicTacToe - Square Query.rq"]
        square_query_seed = ""
        square_query = None
        square_query = read_and_append_strings(file_paths, square_query_seed)
        square_query = square_query.replace("?square_Parameter", f"fact:Square{tictactoe_object}")
     
        square_result = None
        square_result = CurrentGameState_graph.query(square_query)
        square_iri = None
        row = None

        for row in square_result:
            square_iri = f"{row.square}"
        
        # Read the SPARQL query to establish scenario id
        file_paths = ["/tic-tac-toe/queries/TicTacToe - Scenario Query.rq"]
        scenario_query_seed = ""
        scenario_query = None
        scenario_query = read_and_append_strings(file_paths, scenario_query_seed)
        scenario_query = scenario_query.replace("?square_Parameter", "<" + square_iri + ">")
        scenario_query = scenario_query.replace("?actor_class_Parameter", f"fact:Player{tictactoe_actor}")
        scenario_query = scenario_query.replace("?action_Parameter", "'puttingMarkInSquare'")
        scenario_query = scenario_query.replace("?action_class_Parameter", "fact:PuttingMarkInSquare")
        
    elif tictactoe_action == 'declare':
        if tictactoe_object == 'win':
            file_paths = ["/tic-tac-toe/queries/TicTacToe - Object ID Query.rq"]
            object_query_seed = ""
            object_query = None
            object_query = read_and_append_strings(file_paths, object_query_seed)
            object_query = object_query.replace("?object_Parameter", "fact:Win")
            object_result = None
            object_result = CurrentGameState_graph.query(object_query)
            object_iri = None
            for row3 in object_result:
                object_iri = f"{row3.object}"
          
            # Read the SPARQL query to establish scenario id
            file_paths = ["/tic-tac-toe/queries/TicTacToe - Scenario Query.rq"]
            scenario_query_seed = ""
            scenario_query = None
            scenario_query = read_and_append_strings(file_paths, scenario_query_seed)
            scenario_query = scenario_query.replace("?gameStatus_Parameter", "<" + object_iri + ">")
            scenario_query = scenario_query.replace("?actor_class_Parameter", f"fact:Player{tictactoe_actor}")
            scenario_query = scenario_query.replace("?action_Parameter", "'declaringAnOutcomeInAGameOfTicTacToe'")
            scenario_query = scenario_query.replace("?action_class_Parameter", "fact:DeclaringAnOutcomeInAGameOfTicTacToe")

        elif tictactoe_object == 'tie':
            file_paths = ["/tic-tac-toe/queries/TicTacToe - Object ID Query.rq"]
            object_query_seed = ""
            object_query = None
            object_query = read_and_append_strings(file_paths, object_query_seed)
            object_query = object_query.replace("?object_Parameter", "fact:Tie")
            object_result = CurrentGameState_graph.query(object_query)
            object_iri = None
            for row5 in object_result:
                object_iri = f"{row5.object}"
          
            # Read the SPARQL query to establish scenario id
            file_paths = ["/tic-tac-toe/queries/TicTacToe - Scenario Query.rq"]
            scenario_query_seed = ""
            scenario_query = None
            scenario_query = read_and_append_strings(file_paths, scenario_query_seed)
            scenario_query = scenario_query.replace("?gameStatus_Parameter", "<" + object_iri + ">")
            scenario_query = scenario_query.replace("?actor_class_Parameter", f"fact:Player{tictactoe_actor}")
            scenario_query = scenario_query.replace("?action_Parameter", "'declaringAnOutcomeInAGameOfTicTacToe'")
            scenario_query = scenario_query.replace("?action_class_Parameter", "fact:DeclaringAnOutcomeInAGameOfTicTacToe")

    CurrentGameAction = CurrentGameState_graph.query(scenario_query)

    # Create the graph for game action template of the player
    CurrentGameAction_graph = Graph()
    CurrentGameAction_graph.bind("flint", flint)
    CurrentGameAction_graph.bind("fact", fact)
    CurrentGameAction_graph.bind("scenario", scenario)
    CurrentGameAction_graph.bind("RDF", RDF)
    CurrentGameAction_graph.bind("prov", PROV)

    # Read the model of FLint, the model of TicTacToe, and the scenario
    file_paths = [
        "/tic-tac-toe/data/generated/game/TicTacToe - ValidationHistory.ttl",
    ]
    data_graph_seed = ""
    data_graph = read_and_append_strings(file_paths, data_graph_seed)
    

    # Create a graph object to which shapes will be applied
    d = Graph().parse(data=data_graph, format="turtle")
    for triple in CurrentGameAction:
        d.add(triple)

    # Validate the data graph according to the shapes graph
    results = pyshacl.validate(
        d,
        shacl_graph=shapes_graph,
        data_graph_format="turtle",
        shacl_graph_format="turtle",
        advanced=True,
        inplace=True,
        inference="owlrl",
        debug=False,
        serialize_report_graph="turtle",
    )

    conforms, report_graph, report_text = results
    print(report_text)

    shacl_report_graph = Graph()
    shacl_report_graph.bind("flint", flint)
    shacl_report_graph.bind("fact", fact)
    shacl_report_graph.bind("scenario", scenario)
    shacl_report_graph.bind("RDF", RDF)
    shacl_report_graph.bind("prov", PROV)
    shacl_report_graph = Graph().parse(data=report_graph.decode(), format="turtle")
    shacl_report_graph.serialize(destination="tic-tac-toe/data/generated/game/TicTacToe - ValidationReport.ttl", format="turtle")

    file_paths = ["/tic-tac-toe/queries/TicTacToe - Report Status Query.rq"]
    report_status_query_seed = ""
    report_status_query = None
    report_status_query = read_and_append_strings(file_paths, report_status_query_seed)
    CurrentReportStatus = shacl_report_graph.query(report_status_query)
    for row in CurrentReportStatus:
           reportstatus = f"{row.reportstatus}"
    
    
    # Write the inferred knowledge model to a file
    if reportstatus == 'true':
        print(d.serialize(destination="tic-tac-toe/data/generated/game/TicTacToe - ValidationHistory.ttl", format="turtle"))
        
        # Read the SPARQL query to establish the game state
        file_paths = ["/tic-tac-toe/queries/TicTacToe - GameState Select Query.rq"]
        gamestate_select_query_seed = ""
        gamestate_select_query = read_and_append_strings(file_paths, gamestate_select_query_seed)
        resulting_select_gamestate = d.query(gamestate_select_query)
        results_str = ""
        for result in resulting_select_gamestate:

            # Process each result and concatenate to the string
            game_status = result["gameStatus_str"]
            game_winner = result["gameWinnerLabel"]
            player = result["players_str"]
            player_turn = result["playerTurnLabel"]
            square = result["squares"]
              
            # Concatenate the result values to the string
            results_str += f"Game Status: {game_status}\n"
            results_str += f"Game Winner: {game_winner}\n"
            results_str += f"Player: {player}\n"
            results_str += f"Player Turn: {player_turn}\n"
            results_str += f"Gameboard: \n\t{square}\n\t"

        
        print ("")    
        print ("Game metadata:")    
        print ("")
        print (results_str)

	 # Next move
    execute_move()


# Namespace bindings
flint = Namespace("http://ontology.tno.nl/normengineering/flint/model/def/")
fact = Namespace("http://ontology.tno.nl/normengineering/flint/factframe/model/def/")
scenario = Namespace("http://ontology.tno.nl/normengineering/flint/scenario/id/")

# Read the shapes
file_paths = ["/tic-tac-toe/data/static/model/TicTacToe - Shapes.ttl"]
shapes_graph_seed = ""
shapes_graph = read_and_append_strings(file_paths, shapes_graph_seed)

# Reset previous validation report result
file_path = '/tic-tac-toe/data/generated/game/TicTacToe - ValidationReport.ttl'
make_file_empty(file_path)

# Reset previous validation scenario history
file_path = '/tic-tac-toe/data/generated/game/TicTacToe - ValidationHistory.ttl'
make_file_empty(file_path)

# Prepare validation of initial state
file_paths = [
        "/tic-tac-toe/data/static/model/Flint model.ttl",
        "/tic-tac-toe/data/static/model/TicTacToe - ActFrames.ttl",
        "/tic-tac-toe/data/static/model/TicTacToe - FactFrames.ttl",
        "/tic-tac-toe/data/static/model/TicTacToe - Mapping.ttl",
        "/tic-tac-toe/data/static/game/TicTacToe - InitialGameState.ttl",
        "/tic-tac-toe/data/static/game/TicTacToe - ScenarioMetadata.ttl",
    ]
data_graph_seed = ""
data_graph = read_and_append_strings(file_paths, data_graph_seed)

# Create a graph object to which shapes will be applied
d = Graph().parse(data=data_graph, format="turtle")

# Write the inferred knowledge model to a file
print(d.serialize(destination="tic-tac-toe/data/generated/game/TicTacToe - ValidationHistory.ttl", format="turtle"))

# Print new game status to screen
results_str = ""
results_str += f"Game Status: To be started\n"
results_str += f"Game Winner: To be established\n"
results_str += f"Player: Player O, Player X\n"
results_str += f"Player Turn: Player X\n"
results_str += f"Gameboard: \n\tsquare1: empty\n\tsquare2: empty\n\tsquare3: empty\n\tsquare4: empty\n\tsquare5: empty\n\tsquare6: empty\n\tsquare7: empty\n\tsquare8: empty\n\tsquare9: empty\n\t"

print ("")    
print ("Game metadata:")    
print ("")
print (results_str)
  
# start the game
execute_move()
